#!/usr/bin/env python3

import builtins
import os
import inotify.adapters

def openfile(path, retry=False):
    notifier = inotify.adapters.Inotify()

    with builtins.open(path, 'r') as f:
        orig_file_inode = os.fstat(f.fileno()).st_ino
        def file_inode_has_changed():
            try:
                return orig_file_inode != os.stat(path).st_ino
            except FileNotFoundError:
                return True

        yield from f
        notifier.add_watch(path)
        try:
            for event in notifier.event_gen(yield_nones=False):
                (_, type_names, _, _) = event
                if 'IN_MODIFY' in type_names:
                    yield from f
                elif 'IN_MOVE_SELF' in type_names:
                    return
                elif 'IN_ATTRIB' in type_names:
                    if file_inode_has_changed():
                        return
        finally:
            notifier.remove_watch(path)

def openpath(path, retry=False):
    while True:
        yield from openfile(path)

def open(path, follow_name=False, retry=False):
    if follow_name:
        yield from openpath(path, retry)
    else:
        yield from openfile(path, retry)

if __name__ == '__main__':
    import sys
    import argparse

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--follow-name', action="store_true")
    parser.add_argument('--retry', action="store_true")
    parser.add_argument('file_name')
    args = parser.parse_args()

    try:
        for line in open(args.file_name,
                         follow_name=args.follow_name,
                         retry=args.retry):
            print(line, end='')
            sys.stdout.flush()
    except (KeyboardInterrupt, SystemExit):
        print('Interrupted', file=sys.stderr)
        sys.exit(130)
